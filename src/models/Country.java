package models;

import interfaces.Measurable;


public class Country implements Measurable{
	String country ;
	double area ;
	
	public Country(String country, double area){
		this.country = country;
		this.area = area;
	}

	@Override
	public double getMeasure() {
		return area;
	}
	
	public String toString(){
		return country+" >> "+area ;
	}
}