package models;

import interfaces.Taxable;


public class Company implements Taxable{
	private String company ;
	private double income ;
	private double expense ;
	
	public Company(String company, double income, double expense){
		this.company = company;
		this.income = income;
		this.expense = expense;
	}
	
	@Override
	public double getTax() {
		return 0.3*(income - expense);
	}
}
