package models;

import interfaces.Measurable;
import interfaces.Taxable;
public class Person implements Measurable,Taxable{
	private String name;
	private double income;
	private double height;
		
	public Person(String name, double height, double income) {
		this.name = name;
		this.income = income;
		this.height = height;
	}

	@Override
	public double getTax() {
		if (income > 300001) {
			return (300000*0.05)+0.1*(income-300000);
		}
		return income*0.05 ;
	}
	
	@Override
	public double getMeasure() {
		return height;
	}
	
	public String toString(){
		return name+" >> "+height ;
	}
}
