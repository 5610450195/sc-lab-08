package models;

import interfaces.Taxable;

public class Product implements Taxable{
	private String product ;
	private double price ;

	public Product(String product, double price){
		this.product = product;
		this.price = price;
	}

	@Override
	public double getTax() {
		return price*0.07 ;
	}
	
}
