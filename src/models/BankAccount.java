package models;

import interfaces.Measurable;


public class BankAccount implements Measurable{
	String name ;
	double balance ;
	
	public BankAccount(String name, double balance){
		this.name = name;
		this.balance = balance;
	}
	
	public String toString(){
		return name+" >> "+balance ;
	}

	@Override
	public double getMeasure() {
		return balance;
	}
}
