package controller;

import java.util.ArrayList;

import models.BankAccount;
import models.Company;
import models.Country;
import models.Data;
import models.Person;
import models.Product;
import models.TaxCalculator;
import interfaces.Measurable;
import interfaces.Taxable;

public class Test {
	public static void main(String[] args) {
		Test test = new Test();
		test.testPerson();
		test.testMin();
		test.testTax();
	}
	// First answer
	public void testPerson() {
		Measurable[] persons = new Measurable[3];
		persons[0] = new Person("Naru",180, 0);
		persons[1] = new Person("Coco",120, 0);
		persons[2] = new Person("Alice",100, 0);
		System.out.println("Test Average Height >> "+Data.average(persons));
	}
	// Second answer
	public void testMin(){
		Measurable m1, m2, min;
		m1 = new Person("First", 170, 0);
		m2 = new Person("Second", 160, 0);
		System.out.println("--------------Test Min--------------\n"+Data.min(m1,m2));
		
		Measurable[] minBoth = new Measurable[3];
		m1 = new BankAccount("First", 500);
		m2 = new BankAccount("Second", 2000);
		minBoth[1] = Data.min(m1,m2);
		m1 = new Country("Thai", 1000);
		m2 = new Country("USA", 100000);
		minBoth[2] = Data.min(m1,m2);
		System.out.println(minBoth[1]);
		System.out.println(minBoth[2]);
	}
	// Third Answer
	public void testTax() {
		System.out.println("--------------Test Tax--------------");
		ArrayList<Taxable> personal = new ArrayList<Taxable>();
		personal.add(new Person("First", 0, 100000));
		personal.add(new Person("Second", 0, 200000));
		System.out.println("Personal Tax >> " + TaxCalculator.sum(personal));
	
		ArrayList<Taxable> corporate = new ArrayList<Taxable>();
		corporate.add(new Company("First", 1000000 , 1000000));
		corporate.add(new Company("Second", 500000 , 50000));
		System.out.println("Corporate Tax >> " + TaxCalculator.sum(corporate));
	
		ArrayList<Taxable> product = new ArrayList<Taxable>();
		product.add(new Product("First", 80000000));
		product.add(new Product("Second", 5));
		System.out.println("Product Tax >> " + TaxCalculator.sum(product));
	
		ArrayList<Taxable> tax = new ArrayList<Taxable>();
		tax.add(new Person("First", 0, 100000));
		tax.add(new Company("First", 1000000 , 1000000));
		tax.add(new Product("First", 80000000));
		System.out.println("Tax All >> " + TaxCalculator.sum(tax));}
}
